Source: newsboat
Section: net
Priority: optional
Maintainer: Nikos Tsipinakis <nikos@tsipinakis.com>
Build-Depends: debhelper-compat (= 12),
               libncursesw5-dev,
               libxml2-dev,
               libstfl-dev,
               libsqlite3-dev,
               perl,
               pkg-config,
               libcurl4-gnutls-dev,
               libjson-c-dev,
               asciidoctor,
               cargo,
               libbrotli-dev,
               libpsl-dev,
               libgnutls28-dev,
               libidn2-dev,
               libnghttp2-dev,
               libnghttp3-dev,
               libngtcp2-crypto-gnutls-dev,
               libngtcp2-dev,
               libssh2-1-dev,
               librtmp-dev,
               librust-lexopt-0.3-dev,
               librust-md5-0.7-dev,
               librust-bitflags-2-dev,
               librust-chrono-0.4-dev,
               librust-cxx-1-dev,
               librust-cxx-build-1-dev,
               librust-rand-0.8-dev,
               librust-once-cell-1+parking-lot-dev,
               librust-regex-1-dev,
               librust-url-2-dev,
               librust-xdg-2-dev,
               librust-backtrace-0.3-dev,
               librust-backtrace-sys-0.1-dev,
               librust-unicode-width-0.1-dev,
               librust-nom-7-dev,
               librust-curl-sys-0.4+ssl-dev,
               librust-libc-0.2-dev,
               librust-gettext-rs-0.7-dev,
               librust-natord-1-dev,
               librust-gettext-sys-0.21-dev,
               librust-tempfile-3-dev,
               librust-proptest-1+bit-set-dev,
               librust-proptest-1+rusty-fork-dev,
               librust-proptest-1+timeout-dev,
               librust-percent-encoding-2-dev,
               librust-section-testing-0.0.4-dev,
               libssh2-1-dev,
               libzstd-dev,
Standards-Version: 4.5.0
Homepage: https://www.newsboat.org
Vcs-Git: https://salsa.debian.org/nikos/newsboat.git
Vcs-Browser: https://salsa.debian.org/nikos/newsboat
Rules-Requires-Root: no

Package: newsboat
Architecture: any
Recommends: sensible-utils
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: text mode rss feed reader with podcast support
 newsboat is an RSS/Atom feed reader for the text console. It supports OPML
 import/export, podcasts (via companion program podboat), and can serve as
 a client to various feed aggregators (TT-RSS, The Old Reader, Newsblur,
 FeedHQ, ownCloud/nextCloud News). Its interface draws inspiration from mutt
 and slrn.
 .
 Successor of newsbeuter.
